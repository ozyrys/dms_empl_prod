/**
 * @file
 */

(function ($) {
    Drupal.behaviors.dms = {
        attach: function (context, settings) {
            $('.clickable').click(function () {
                // Var icon = $(this).closest('div.pull-right').find('.glyphicon');
                var icon = $(this).parent('p').siblings('div.pull-right').find('.glyphicon');
                if (icon.length > 0 && icon.hasClass('glyphicon-chevron-up')) {
                    $('i.glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    icon.removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    return true;
                }
                if (icon.length > 0 && icon.hasClass('glyphicon-chevron-down')) {
                    icon.removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    return true;
                }
            });
        }
    };

})(jQuery);
