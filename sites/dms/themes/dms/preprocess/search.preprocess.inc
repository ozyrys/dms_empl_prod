<?php

/**
 * @file
 * search.preprocess.inc
 *
 * All preprocess functions related to "search" placeholder live in this file.
 */

/**
 * Implements hook_form_FORM_ID_alter() for search_block_form().
 */
function dms_form_search_block_form_alter(&$form, &$form_state) {
  $form['actions']['submit']['#src'] = drupal_get_path('theme', 'dms')
    . '/images/search.png';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dms_form_apachesolr_search_custom_page_search_form_alter(&$form, &$form_state) {
  $form['basic']['submit']['#src'] = drupal_get_path('theme', 'dms')
    . '/images/search.png';
}

/**
 * Implements hook_form_FORM_ID_alter() for search_form().
 */
function dms_form_search_form_alter(&$form, &$form_state) {
  $form['basic']['submit']['#src'] = drupal_get_path('theme', 'dms')
    . '/images/search.png';
}
