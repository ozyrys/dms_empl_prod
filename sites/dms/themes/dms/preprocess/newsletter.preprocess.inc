<?php

/**
 * @file
 * newsletter.preprocess.inc
 *
 * All preprocess functions related to "newsletter" live in this file.
 */

/**
 * Process variables to format the simplenews newsletter body.
 *
 * @see simplenews-newsletter-body.tpl.php
 *
 * @ingroup theming
 */
function dms_preprocess_simplenews_newsletter_body(&$variables) {
  $variables['heder_bg'] = file_create_url(drupal_get_path('theme', 'dms') . '/images/dms-newsletter-header-belt-bg.png');
  $variables['header'] = file_create_url(drupal_get_path('theme', 'dms') . '/images/dms-newsletter-header.png');
  $variables['logo'] = file_create_url(drupal_get_path('theme', 'dms') . '/images/dms-newsletter-logo.png');
  $variables['banner'] = file_create_url(drupal_get_path('theme', 'dms') . '/images/dms-newsletter-banner.jpg');

  $variables['site_name'] = (theme_get_setting('toggle_name') ? filter_xss_admin(variable_get('site_name', 'Drupal')) : '');
  $variables['site_slogan'] = (theme_get_setting('toggle_slogan') ? filter_xss_admin(variable_get('site_slogan', '')) : '');

  global $_om_maximenu_variable;
  $variables['menu_links'] = '';
  foreach ($_om_maximenu_variable[1]['links'] as $link) {
    $variables['menu_links'] .= '<span> |</span>' . l($link['link_title'], $link['path']);
  }
}
