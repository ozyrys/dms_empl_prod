/**
 * @file
 * Where we assemble modules and export the store.
 */

import Vue from "vue";
import Vuex from "vuex";
import config from "./modules/config";
import publications from "./modules/publications";
import loader from "./modules/loader";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    config,
    publications,
    loader
  },
  strict: process.env.NODE_ENV !== "production"
});
