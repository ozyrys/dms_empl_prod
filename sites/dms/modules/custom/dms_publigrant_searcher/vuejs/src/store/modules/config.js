/**
 * @file
 * Store module to handle Publications lists.
 */

import Vue from "vue";
import store from "../store";

export default {
  namespaced: true,
  // ----------------------------------------------------------------------------------.
  state: {
    programmes: [],
    countries: []
  },
  // ----------------------------------------------------------------------------------.
  getters: {
    programmes: state => state.programmes,
    countries: state => state.countries
  },
  // ----------------------------------------------------------------------------------.
  mutations: {
    setProgrammes: (state, programmes) => {
      // Uses Vue.set to be sure to be deeply reactive.
      Vue.set(state, "programmes", programmes);
    },
    setCountries: (state, countries) => {
      Vue.set(state, "countries", countries);
    }
  },
  // ----------------------------------------------------------------------------------.
  actions: {
    getProgrammes: context => {
      store.commit("loader/START_LOADING");
      // don't forget that an action should return a promise.
      return Vue.axios
        .get("Programmes/" + Vue.prototype.$wsv)
        .then(response => {
          store.commit("loader/FINISH_LOADING");
          context.commit("setProgrammes", response.data);
          return Promise.resolve(context.state.programmes);
        })
        .catch(error => {
          store.commit("loader/FINISH_LOADING");
          // In case of error, empties the publications collection.
          context.commit("setProgrammes", {});
          return Promise.reject(error);
        });
    },
    getCountries: context => {
      store.commit("loader/START_LOADING");
      // Don't forget that an action should return a promise.
      return Vue.axios
        .get("Countries/" + Vue.prototype.$wsv)
        .then(response => {
          store.commit("loader/FINISH_LOADING");
          context.commit("setCountries", response.data);
          return Promise.resolve(context.state.countries);
        })
        .catch(error => {
          store.commit("loader/FINISH_LOADING");
          // In case of error, empties the countries collection.
          context.commit("setCountries", {});
          return Promise.reject(error);
        });
    }
  }
};
