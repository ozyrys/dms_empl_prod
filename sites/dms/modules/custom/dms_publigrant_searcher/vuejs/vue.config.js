/**
 * @file
 * Vue config.
 */

module.exports = {
  runtimeCompiler:true,
  devServer: {
    host: '0.0.0.0',
    port: 3000,
    public:'0.0.0.0:3333'
  },
  publicPath: '/tools/dms/sites/default/modules/custom/dms_publigrant_searcher/vuejs/'
}
