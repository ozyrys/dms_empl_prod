/**
 * @file
 */

(function ($) {
    Drupal.behaviors.dms_frontpage = {
        attach: function (context, settings) {
            // Fixes for the front page block, now the layout will be fluid.
            $(".page-node-1 #layout-body").removeClass('container');
            $(".page-node-1 #layout-body").addClass('container-fluid');
            $(".page-node-1 #layout-body #block-print-ui-print-links").hide();
        }
  };
})(jQuery);
