<?php

/**
 * @file
 * Administration for the DMS DG Filter Feature.
 */

/**
 * Implements hook_admin_settings_form().
 */
function dms_dgfilter_admin_settings_form($form, &$form_state) {

  $levels = array(
    'status' => t('Status'),
    'warning' => t('Warning'),
    'error' => t('Error'),
  );

  $form['dms_dgfilter_restricted_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Restricted Content Messages'),
    '#collapsible' => FALSE,
  );

  $form['dms_dgfilter_restricted_message']['dms_dgfilter_no_access'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to tell the user he/she has no access to the content.'),
    '#default_value' => variable_get('dms_dgfilter_no_access', ''),
    '#required' => FALSE,
    '#size' => 120,
    '#maxlength' => 254,
    '#description' => t('Type the message which will tell the user about the no access.'),
  );
  $form['dms_dgfilter_restricted_message']['dms_dgfilter_no_access_status'] = array(
    '#type' => 'select',
    '#title' => t('Status of "No Access" message.'),
    '#default_value' => variable_get('dms_dgfilter_no_access_status'),
    '#required' => FALSE,
    '#options' => $levels,
    '#description' => t('Level of the message (status/warning/error).'),
  );

  $form['dms_dgfilter_restricted_message']['dms_dgfilter_no_access_reports'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to tell the user he/she has no access to reports.'),
    '#default_value' => variable_get('dms_dgfilter_no_access_reports', ''),
    '#required' => FALSE,
    '#size' => 120,
    '#maxlength' => 254,
    '#description' => t('Type the message which will tell the user about the no access to reports.'),
  );
  $form['dms_dgfilter_restricted_message']['dms_dgfilter_no_access_reports_status'] = array(
    '#type' => 'select',
    '#title' => t('Status of "No Access" to Reports message.'),
    '#default_value' => variable_get('dms_dgfilter_no_access_reports_status'),
    '#required' => FALSE,
    '#options' => $levels,
    '#description' => t('Level of the message (status/warning/error).'),
  );

  $form['dms_dgfilter_restricted_message']['dms_dgfilter_what_to_do'] = array(
    '#type' => 'textfield',
    '#title' => t('Message to tell the user what he/she can do.'),
    '#default_value' => variable_get('dms_dgfilter_what_to_do', ''),
    '#required' => FALSE,
    '#size' => 120,
    '#maxlength' => 254,
    '#description' => t('Type the message which will tell the user what to do to have access.'),
  );
  $form['dms_dgfilter_restricted_message']['dms_dgfilter_what_to_do_status'] = array(
    '#type' => 'select',
    '#title' => t('Status of "What to do" message.'),
    '#default_value' => variable_get('dms_dgfilter_what_to_do_status'),
    '#required' => FALSE,
    '#options' => $levels,
    '#description' => t('Level of the message (status/warning/error).'),
  );

  $form['dms_dgfilter_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('DMS Sections Configuration'),
    '#collapsible' => FALSE,
  );

  $form['dms_dgfilter_configuration']['dms_dgfilter_vocabulary_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machiname of the DMS vocabulary'),
    '#default_value' => variable_get('dms_dgfilter_vocabulary_name', ''),
    '#required' => FALSE,
    '#size' => 120,
    '#maxlength' => 254,
    '#description' => t('Specify the machine name of the DMS vocabulary.'),
  );

  $form['dms_dgfilter_configuration']['dms_dgfilter_roles_allowed'] = array(
    '#type' => 'select',
    '#title' => t('Roles allowed to see Private content.'),
    '#default_value' => variable_get('dms_dgfilter_roles_allowed'),
    '#required' => FALSE,
    '#options' => user_roles(),
    '#multiple' => TRUE,
    '#description' => t('Roles allowed to see the content marked as private.'),
  );

  $form['dms_dgfilter_configuration']['dms_dgfilter_dgempl_role'] = array(
    '#type' => 'select',
    '#title' => t('DG Employment role name.'),
    '#default_value' => variable_get('dms_dgfilter_dgempl_role', 0),
    '#required' => FALSE,
    '#options' => user_roles(),
    '#multiple' => FALSE,
    '#chosen' => TRUE,
    '#description' => t('Role allowed to see the reports and asigned automatically if the user pertains to DG Employment.'),
  );

  $form['dms_dgfilter_term_block_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('DMS Term Block Configuration'),
    '#collapsible' => FALSE,
  );
  $blocks = _dms_dgfilter_get_block_list();
  $terms = _dms_dgfilter_get_term_list();
  foreach ($terms as $tid => $term) {
    $form['dms_dgfilter_term_block_configuration']['dms_dgfilter_term_blocks_' . $tid] = array(
      '#type' => 'select',
      '#title' => t('Block related to the %term term .', ['%term' => $term]),
      '#default_value' => variable_get('dms_dgfilter_term_blocks_' . $tid),
      '#required' => FALSE,
      '#options' => $blocks,
      '#multiple' => FALSE,
      '#chosen' => TRUE,
      '#description' => t('Block related to the term.'),
    );
  }

  return system_settings_form($form);
}

/**
 * Returns an array with the simplified block list.
 *
 * @return array
 *   Associative array with the block delta and block name.
 */
function _dms_dgfilter_get_block_list() {
  module_load_include('inc', 'block', 'block.admin');
  global $theme_key;
  $all_blocks = block_admin_display_prepare_blocks($theme_key);
  $block_list = [];
  foreach ($all_blocks as $block) {
    $block_list[$block['delta']] = $block['info'];
  }
  return $block_list;
}

/**
 * Returns an array with the simplified vocabulary term list.
 *
 * @return array
 *   Associative array with the term tid and term name.
 */
function _dms_dgfilter_get_term_list() {
  $vname = variable_get('dms_dgfilter_vocabulary_name', '');
  $terms = [];
  if (!empty($vname)) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($vname);
    $terms_tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($terms_tree as $term) {
      $terms[$term->tid] = $term->name;
    }
  }
  return $terms;
}
