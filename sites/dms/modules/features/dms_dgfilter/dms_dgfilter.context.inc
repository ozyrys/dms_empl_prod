<?php

/**
 * @file
 * dms_dgfilter.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function dms_dgfilter_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'bvf';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        44 => 44,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/budget-versions-for-financial-officers-bvf/*' => 'dms/budget-versions-for-financial-officers-bvf/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-bvf',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['bvf'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'coli';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        45 => 45,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/contracts-on-line-coli/*' => 'dms/contracts-on-line-coli/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-coli',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['coli'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'coli2';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        65 => 65,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/contracts-on-line-v2--coli2/*' => 'dms/contracts-on-line-v2--coli2/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-coli2',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['coli2'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'defis';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        40 => 40,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/defis/*' => 'dms/defis/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-defis',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['defis'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'dms_home';
  $context->description = 'DMS Homepage for authenticated users';
  $context->tag = 'DMS Homepage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
    'user' => array(
      'values' => array(
        'DG Employment' => 'DG Employment',
        'EC' => 'EC',
        'administrator' => 'administrator',
        'editor' => 'editor',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-22' => array(
          'module' => 'block',
          'delta' => '22',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('DMS Homepage');
  t('DMS Homepage for authenticated users');
  $export['dms_home'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'dms_home_anonymous';
  $context->description = 'DMS homepage for anonymous';
  $context->tag = 'DMS Homepage';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        '~dms_home' => '~dms_home',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-27' => array(
          'module' => 'block',
          'delta' => '27',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('DMS Homepage');
  t('DMS homepage for anonymous');
  $export['dms_home_anonymous'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'external-expert';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        49 => 49,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/external-expert/*' => 'dms/external-expert/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-external-expert',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['external-expert'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'finap';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        61 => 61,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/finap/*' => 'dms/finap/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-finap',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['finap'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'publigrant';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        48 => 48,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/publigrant/*' => 'dms/publigrant/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-publigrant',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['publigrant'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'dms/reports/*' => 'dms/reports/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-reports',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['reports'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'requests';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'dms/defis-request/*' => 'dms/defis-request/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-requests',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['requests'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'swim';
  $context->description = '';
  $context->tag = 'sections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        46 => 46,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'dms/subventions-web-input-module-swim/*' => 'dms/subventions-web-input-module-swim/*',
      ),
    ),
  );
  $context->reactions = array(
    'theme_html' => array(
      'class' => 'section-swim',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sections');
  $export['swim'] = $context;

  return $export;
}
