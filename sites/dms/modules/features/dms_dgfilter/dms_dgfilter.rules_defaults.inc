<?php

/**
 * @file
 * dms_dgfilter.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function dms_dgfilter_default_rules_configuration() {
  $items = array();
  $items['rules_add_user_role_ec_when_email_address_domain_is_ext_ec_europ'] = entity_import('rules_config', '{ "rules_add_user_role_ec_when_email_address_domain_is_ext_ec_europ" : {
      "LABEL" : "Add user role \\u0022EC\\u0022 when email address domain is (ext.)ec.europa.eu",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_presave" : [] },
      "IF" : [
        { "OR" : [
            { "text_matches" : {
                "text" : [ "account:mail" ],
                "match" : "@ext.ec.europa.eu",
                "operation" : "ends"
              }
            },
            { "text_matches" : {
                "text" : [ "account:mail" ],
                "match" : "@ec.europa.eu",
                "operation" : "ends"
              }
            }
          ]
        }
      ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "7" : "7" } } } }
      ]
    }
  }');
  $items['rules_remove_user_role_ec_when_email_address_domain_is_not_ext_e'] = entity_import('rules_config', '{ "rules_remove_user_role_ec_when_email_address_domain_is_not_ext_e" : {
      "LABEL" : "Remove user role \\u0022EC\\u0022 when email address domain is not (ext.)ec.europa.eu",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_presave" : [] },
      "IF" : [
        { "NOT OR" : [
            { "text_matches" : {
                "text" : [ "account:mail" ],
                "match" : "@ext.ec.europa.eu",
                "operation" : "ends"
              }
            },
            { "text_matches" : {
                "text" : [ "account:mail" ],
                "match" : "@ec.europa.eu",
                "operation" : "ends"
              }
            }
          ]
        }
      ],
      "DO" : [
        { "user_remove_role" : { "account" : [ "account" ], "roles" : { "value" : { "7" : "7" } } } }
      ]
    }
  }');
  return $items;
}
