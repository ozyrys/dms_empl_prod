<?php

/**
 * @file
 * dms_dgfilter.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dms_dgfilter_user_default_roles() {
  $roles = array();

  // Exported role: DG Employment.
  $roles['DG Employment'] = (object) array(
    'name' => 'DG Employment',
    'weight' => 2,
  );

  // Exported role: EC.
  $roles['EC'] = (object) array(
    'name' => 'EC',
    'weight' => 5,
  );

  return $roles;
}
