<?php
/**
 * @file
 * dms_qg.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function dms_qg_filter_default_formats() {
  $formats = array();

  // Exported format: Quick Guides.
  $formats['quick_guides'] = (object) array(
    'format' => 'quick_guides',
    'name' => 'Quick Guides',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'toolbox_sanitize' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'media_filter' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -40,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
