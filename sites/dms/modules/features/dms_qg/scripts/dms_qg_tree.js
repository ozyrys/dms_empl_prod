/**
 * @file
 * Dms_qg_tree.js.
 */

(function ($) {
    Drupal.behaviors.dms_qg = {
      attach: function (context, settings) {
        let index = '';
        let tocindex = 0;
        let tocobj = new Object();
        tocobj.pos = 0;
        $("#toc").tocify({
          selectors: "h2, h3, h4, h5, h6, h7, h8, h9, h10, h11, h12, h13",
          scrollTo: 30,
          scrollHistory: true,
          hashGenerator: function (text, element) {
            return prettyGen(text, element);
          }
        }).data("toc-tocify");
        $(".optionName").popover({
          trigger: "hover"
        });

        // HACK TO PREVENT THE TOC DEFORMATION WHEN DATA-SPY AFFIX ENTERS.
        let width = $('#toc').width() + 20;
        $('#toc').css('width', width);
        let top = parseInt($('#toc').offset().top);

        $(document).scroll(function (event) {
          let offset = parseInt($('#toc').offset().top);
          if (offset > top) {
            $('#toc').css('width', width);
          }
        });

        /**
         * Generates the TOC anchors in a pretty format, complete words and - for spaces.
         *
         * @param {string} text    Text of the tag element
         * @param {Object} element The element tag.
         *
         * @return {string}         Hash for the anchor TOC
         */
        function prettyGen(text, element) {
          // Prettify the text.
          hashValue = text.toLowerCase().replace(/\s/g, "-");

          // Fix possible duplicates.
          hashValue = hashValue + '-' + tocindex;

          // Increment the index only when necessary.
          if (hashValue.indexOf("primary-tabs") == -1 && hashValue.indexOf("status-message") == -1) {
            tocindex++;
          }

          // Fix double hyphens.
          while (hashValue.indexOf("--") > -1) {
            hashValue = hashValue.replace(/--/g, "-");
          }

          // Fix colon-space instances.
          while (hashValue.indexOf(":-") > -1) {
            hashValue = hashValue.replace(/:-/g, "-");
          }

          // Fix long dashes.
          hashValue = hashValue.replace(/—/g, "-");
          hashValue = hashValue.replace(/–/g, "-");

          return hashValue;
        }

        /**
         * Generates the TOC anchors in a compact format, complete words without spaces.
         *
         * @param {string} text    Text of the tag element
         * @param {Object} element The element tag.
         *
         * @return {string}         Hash for the anchor TOC
         */
        function compactGen(text, element) {
          let hash = text.replace(/\s/g, "") + '-' + tocindex;
          tocindex++;
          return hash;
        }

        /**
         * Generates the TOC anchors in a numeric format, numbers and levels.
         *
         * @param {string} text    Text of the tag element
         * @param {Object} element The element tag.
         *
         * @return {string}         Hash for the anchor TOC
         */
        function numericTreeGen(text, element) {
          let header = _getHeaderNumber(element);
          let h = 'h' + header;
          if (tocobj.pos == 0) {
            tocobj.pos = header;
          }
          if (tocobj.pos <= header) {
            // One level inside (<) or same level (==).
            if (tocobj.pos < header) {
              tocobj.pos = header;
            }
            if (tocobj[h] == NaN || tocobj[h] == undefined) {
              tocobj[h] = 1;
            }
            else {
              tocobj[h] += 1;
            }
          }
          else {
            // Going one level up, erase everything from below.
            tocobj.pos = header;
            tocobj[h] += 1;
            Object.keys(tocobj).forEach(function (key, i) {
              if (key != 'pos' && _getInt(key) > tocobj.pos) {
                tocobj[key] = 0;
              }
            });
          }

          // Now join the values using the pos and the h attribs.
          index = 's';
          Object.keys(tocobj).forEach(function (key, i) {
            if (key != 'pos') {
              if (tocobj[key] > 0) {
                index = index + tocobj[key] + '-';
              }
            }
          });
          // Remove the trailing '-'.
          index = index.slice(0, -1);
          return index;
        }

        /**
         * Gets the integer part of the header tag.
         *
         * @param {Object} element HTML Tag element
         *
         * @return {integer}         Number of the header tag
         */
        function _getHeaderNumber(element) {
          return _getInt($(element).prop('tagName'));
        }

        /**
         * Filters the text and returns only the integer part.
         *
         * @param {string} text
         *
         * @return {integer}
         */
        function _getInt(text) {
          return text.match(/\d+/)[0];
        }

        /**
         * Go back to Support Materials page button.
         */

        /**
         * Hide/Show the go back to support materials button.
         */
        $(window).scroll(function () {
          if ($(this).scrollTop() > 200) {
            $('.btn-back-to-page').fadeIn(200);
          }
          else {
            $('.btn-back-to-page').fadeOut(200);
          }
        });

      }
    };
  })(jQuery);
