<?php

/**
 * @file
 * Administration for the DMS Report.
 */

/**
 * Implements hook_admin_settings_form().
 */
function dms_report_settings($form, &$form_state) {

  $form['dms_report_ws_url'] = array(
    '#type' => 'textfield',
    '#title' => t('DMS Report WS URL'),
    '#default_value' => variable_get('dms_report_ws_url', ''),
    '#size' => 128,
    '#maxlength' => 255,
    '#description' => t('Common part of WS URL for DMS Reports'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
