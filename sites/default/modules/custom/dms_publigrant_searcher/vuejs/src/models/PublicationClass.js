/**
 * @file
 * Model definition file for the Publication Class.
 */

import Vue from "vue";
import store from "../store/store";
const _ = require("lodash");

export default class Publication {
  constructor(rawData = {}) {
    // Init publication.
    this.id = rawData.id;
    this.directTargetGroups = "---";
    this.ultimateTargetGroups = "---";
    this.policyAreas = "---";
    this.partners = "---";
    this.organisation = "---";
    this.details = {
      reference: "---",
      endDate: "---",
      finalEuContribution: "0",
      finalTotalCost: "0",
      initialEuContribution: "0",
      initialTotalCost: "0",
      selectionProcedureRef: "---",
      startDate: "---"
    };
    this.translation = [
      {
        language: "en",
        title: "PubId <b>" + rawData.id + "</b> INFO NOT RETRIEVED",
        title_sort: "PubId <b>" + rawData.id + "</b> INFO NOT RETRIEVED",
        summary: "INFO NOT RETRIEVED"
      }
    ];
  }

  static subrequests() {
    // Subrequests necessary to get details about the publication.
    return [
      "translation",
      "countriesInvolved",
      "organisation",
      "partners",
      "directTargetGroups",
      "ultimateTargetGroups",
      "policyAreas"
    ];
  }

  set_publication_details(id) {
    let subrequests = this.constructor.subrequests();
    var promisses = [];
    // Subrequests.
    for (const subrequest of subrequests) {
      promisses.push(
        Vue.axios
          .get(
            "/Publications/" + subrequest + "/" + Vue.prototype.$wsv + "/" + id,
            {
              data: {
                publicationId: id,
                subrequest: subrequest
              }
            }
          )
          .catch(function () {})
      );
    }
    // Details.
    promisses.push(
      Vue.axios
        .get("Publications/" + Vue.prototype.$wsv + "/" + id, {
          data: {
            publicationId: id,
            subrequest: "details"
          }
        })
        .catch(function () {})
    );

    // Promise that ensures, that all asynchronous requests will be done.
    return new Promise((resolve, reject) => {
      Vue.axios
        .all(promisses)
        .then(function (publication_results) {
          let data = {};
          publication_results.forEach(function (results) {
            if (results) {
              let obj = JSON.parse(results.config.data);
              // Preprocess some requests.
              if (
                [
                  "directTargetGroups",
                  "ultimateTargetGroups",
                  "policyAreas"
                ].indexOf(obj.subrequest) != -1
              ) {
                var preprocessed = "";
                for (let item of results.data) {
                  if (_.get(item, "descriptionEn")) {
                    preprocessed +=
                      '<span class="tag">' + item.descriptionEn + "</span>";
                  }
                }
                if (preprocessed == "") {
                  preprocessed = "---";
                }
                results.data = preprocessed;
              }
              else if (obj.subrequest == "partners" && results.data[0]) {
                results.data.forEach(function (value, i) {
                  // Set it collapsed.
                  results.data[i]["collapsed"] = true;
                  // Set human readable country name.
                  let country_obj = store.getters["config/countries"].filter(
                    obj => {
                      return obj.id == value.countryId;
                    }
                  );
                  if (_.get(country_obj, "[0].descriptionEn")) {
                    results.data[i]["country"] = country_obj[0].descriptionEn;
                  }
                });
              }
              else if (obj.subrequest == "organisation") {
                // Set human readable country name.
                let country_obj = store.getters["config/countries"].filter(
                  obj => {
                    return obj.id == results.data.countryId;
                  }
                );
                if (_.get(country_obj, "[0].descriptionEn")) {
                  results.data["country"] = country_obj[0].descriptionEn;
                }
              }
              else if (obj.subrequest == "translation") {
                results.data.forEach(function (value, i) {
                  // Process title to have it clean for sorting.
                  results.data[i].title_sort = results.data[i].title.replace(
                    /[^a-zA-Z ]/g,
                    ""
                  );
                  // Process html new lines in summary.
                  results.data[i].summary = results.data[i].summary.replace(
                    /\n/g,
                    "<br />"
                  );
                });
              }
              data[obj.subrequest] = results.data;
            }
          });
          resolve(data);
        })
        .catch(function (error) {
          reject(error);
        });
    });
  }

  get programme() {
    let programme = "---";
    const programmes = store.getters["config/programmes"];
    if (this.details.programmeId && programmes) {
      let programme_obj = programmes.filter(obj => {
        return obj.id == this.details.programmeId;
      });
      if (_.get(programme_obj, "[0].descriptionEn")) {
        programme = programme_obj[0].descriptionEn;
      }
    }
    return programme;
  }
  get countries() {
    let countries_result = "";
    const countries = store.getters["config/countries"];
    if (this.details.programmeId && countries && this.countriesInvolved) {
      for (let country of this.countriesInvolved) {
        let country_obj = countries.filter(obj => {
          return obj.id == country;
        });
        if (_.get(country_obj, "[0].descriptionEn")) {
          countries_result +=
            "" +
            '<span class="tag">' +
            country_obj[0].descriptionEn +
            "</span>";
        }
      }
      if (countries_result === "") {
        countries_result = "---";
      }
    }
    return countries_result;
  }
}
