/**
 * @file
 * Store module to handle Publications lists.
 */

import Vue from "vue";
import Publication from "../../models/PublicationClass";
const _ = require("lodash");

import store from "./../store";

export default {
  namespaced: true,
  // ----------------------------------------------------------------------------------.
  state: {
    publications: {
      publications: [],
      pagecount: 0
    },
    cached_publications: {},
    publication: {
      id: false
    }
  },
  // ----------------------------------------------------------------------------------.
  getters: {
    collection: state => state.publications,
    cached_collection: state => state.cached_publications,
    publication: state => state.publication
  },
  // ----------------------------------------------------------------------------------.
  mutations: {
    setPublications: (state, payload) => {
      // Uses Vue.set to be sure to be deeply reactive.
      Vue.set(state, "publications", payload.publications);
      Vue.set(state, "cached_publications", payload.cached_publications);
    },
    setPublicationDetails: (state, publication) => {
      let { object, prop, value } = publication;
      object[prop] = value;
    },
    setPublication: (state, publication) => {
      // Uses Vue.set to be sure to be deeply reactive.
      Vue.set(state, "publication", publication);
    }
  },
  // ----------------------------------------------------------------------------------.
  actions: {
    search: (context, params) => {
      store.commit("loader/START_LOADING");
      if (!params.programmeId) {
        store.commit("loader/FINISH_LOADING");
        context.commit("setPublications", []);
        return Promise.resolve([]);
      }
      let size = 10;
      params.page--;
      // Get cache.
      let cachedCollection = context.state.cached_publications;
      if (
        _.get(
          cachedCollection,
          "[" +
            params.programmeId +
            "][" +
            params.page +
            "][" +
            size +
            "]"
        )
      ) {
        store.commit("loader/FINISH_LOADING");
        let newCollection =
          cachedCollection[params.programmeId][params.page][size];
        // Uses Vue.set to be sure to be deeply reactive.
        context.commit("setPublications", {
          publications: newCollection,
          cached_publications: cachedCollection
        });
        return Promise.resolve(context.state.publications);
      }
      // An action should return a promise.
      return Vue.axios
        .get(
          "Publications/" +
            Vue.prototype.$wsv +
            "/search?page=" +
            params.page +
            "&ocurrencePerPage=" +
            size +
            "&programmeId=" +
            params.programmeId
        )
        .then(response => {
          store.commit("loader/FINISH_LOADING");
          // Convert all raw data publications to Publication class.
          let newCollection = {};
          newCollection["publications"] = [];
          // Page count.
          if (size) {
            store.commit("loader/START_LOADING");
            Vue.axios
              .get(
                "Publications/" +
                  Vue.prototype.$wsv +
                  "/getNumberOfPages?ocurrencePerPage=" +
                  size +
                  "&programmeId=" +
                  params.programmeId,
                {
                  data: {
                    subrequest: "pagecount"
                  }
                }
              )
              .then(pagecount_response => {
                store.commit("loader/FINISH_LOADING");
                newCollection["pagecount"] = pagecount_response.data;
                for (const publicationId of response.data) {
                  let publication = new Publication({ id: publicationId });
                  newCollection["publications"].push(publication);
                  store.commit("loader/START_LOADING");
                  publication
                    .set_publication_details(publicationId)
                    .then(data => {
                      store.commit("loader/FINISH_LOADING");
                      let subrequests = Publication.subrequests();
                      subrequests.push("details");
                      for (let subrequest of subrequests) {
                        if (data[subrequest]) {
                          // Update publications.
                          context.commit("setPublicationDetails", {
                            object: publication,
                            prop: subrequest,
                            value: data[subrequest]
                          });
                        }
                      }
                    })
                    .catch(function () {
                      store.commit("loader/FINISH_LOADING");
                    });
                }
                // Set cache.
                _.set(
                  cachedCollection,
                  params.programmeId + "." + params.page + "." + size,
                  newCollection
                );
                // Uses Vue.set to be sure to be deeply reactive.
                context.commit("setPublications", {
                  publications: newCollection,
                  cached_publications: cachedCollection
                });
                return Promise.resolve(context.state.publications);
              });
          }
        })
        .catch(error => {
          store.commit("loader/FINISH_LOADING");
          // In case of error, empties the movies collection.
          context.commit("setPublications", []);
          return Promise.reject(error);
        });
    },

    getPublicationById: (context, publicationId) => {
      // Check if publication id is a number.
      var patt = new RegExp(/^\+?(0|[1-9]\d*)$/);
      if (!patt.test(publicationId)) {
        context.commit("setPublication", { message: "Wrong publication id." });
        return Promise.resolve({ message: "Wrong publication id." });
      }
      store.commit("loader/START_LOADING");
      // Check if publication is already in collection.
      if (
        context.state.publications &&
        context.state.publications["publications"]
      ) {
        let publication = context.state.publications["publications"].filter(
          obj => {
            return obj.id == publicationId;
          }
        );
        if (publication[0]) {
          store.commit("loader/FINISH_LOADING");
          context.commit("setPublication", publication[0]);
          return Promise.resolve(context.state.publication);
        }
      }

      // An action should return a promise.
      return Vue.axios
        .get("Publications/v1/" + publicationId)
        .then(response => {
          store.commit("loader/FINISH_LOADING");
          if (response.data.id) {
            store.commit("loader/START_LOADING");
            let publication = new Publication({ id: publicationId });
            // Convert all raw data publication to Publication class.
            publication.set_publication_details(publicationId).then(data => {
              store.commit("loader/FINISH_LOADING");
              let subrequests = Publication.subrequests();
              context.commit("setPublicationDetails", {
                object: publication,
                prop: "details",
                value: response.data
              });
              for (let subrequest of subrequests) {
                if (data[subrequest]) {
                  // Update publication.
                  context.commit("setPublicationDetails", {
                    object: publication,
                    prop: subrequest,
                    value: data[subrequest]
                  });
                }
              }
            });
            // Uses Vue.set to be sure to be deeply reactive.
            context.commit("setPublication", publication);
            return Promise.resolve(context.state.publication);
          }
          else {
            context.commit("setPublication", {
              message: "404 - Publication not found."
            });
            return Promise.resolve({
              message: "404 - Publication not found."
            });
          }
        })
        .catch(error => {
          store.commit("loader/FINISH_LOADING");
          // In case of error, empties the movies collection.
          context.commit("setPublication", {
            message: "Service temporary unavailable. Please try again later."
          });
          return Promise.reject(error);
        });
    },
    updatePublication: (context, publication) => {
      context.commit("setPublication", publication);
      return Promise.resolve(context.state.publication);
    }
  }
};
