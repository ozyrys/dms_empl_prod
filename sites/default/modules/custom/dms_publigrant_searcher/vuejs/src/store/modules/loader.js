/**
 * @file
 * Store module to handle loading progress lists.
 */

export default {
  namespaced: true,
  // ----------------------------------------------------------------------------------.
  state: {
    loading: 0
  },
  // ----------------------------------------------------------------------------------.
  getters: {
    loading: state => state.loading
  },
  // ----------------------------------------------------------------------------------.
  mutations: {
    START_LOADING: state => state.loading++,
    FINISH_LOADING: state => state.loading--
  }
};
