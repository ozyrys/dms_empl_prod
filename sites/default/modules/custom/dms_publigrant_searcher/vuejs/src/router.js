/**
 * @file
 * Application routing file.
 */

import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Publications.vue";
import Publication from "./views/Publication.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      components: {
        default: Home
      }
    },
    {
      path: "/publication/:id",
      name: "publication",
      components: {
        default: Publication
      }
    }
  ]
});
