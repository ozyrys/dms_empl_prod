/**
 * @file
 * Application main js file.
 */

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);
import Vue2Filters from "vue2-filters";
Vue.use(Vue2Filters);

Vue.config.productionTip = false;

Vue.axios.defaults.baseURL = "https://www.test.cc.cec/dms/publigrant/";
Vue.prototype.$wsv = "v1";

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
