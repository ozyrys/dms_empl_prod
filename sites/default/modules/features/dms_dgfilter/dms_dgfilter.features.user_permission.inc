<?php

/**
 * @file
 * dms_dgfilter.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dms_dgfilter_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer dgilter'.
  $permissions['administer dgilter'] = array(
    'name' => 'administer dgilter',
    'roles' => array(
      'administrator' => 'administrator',
      'structure administrator' => 'structure administrator',
    ),
    'module' => 'dms_dgfilter',
  );

  return $permissions;
}
