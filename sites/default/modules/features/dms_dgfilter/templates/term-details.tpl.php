<?php

  /**
   * @file
   * Theme implementation to display a taxonomy term.
   */
?>
<div id="pubsea-details" class="<?php print $classes; ?>
  clearfix" <?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php print render($title_suffix); ?>
  <div class="content clearfix" <?php print $content_attributes; ?>>
    <?php if (!empty($prefix_display)): ?>
      <div class="node-private label label-default clearfix">
        <span class="glyphicon glyphicon-lock"></span>
        <?php print t('This content is private'); ?>
      </div>
    <?php endif; ?>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    ?>
    <div class="row">
      <div class="col-xs-12">
        <div id="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <?php if ($image): ?>
                      <img class="img-responsive img-rounded" src="<?php print file_create_url($image); ?>" />
                    <?php endif; ?>
                </div>
                <div class="col-xs-12 col-sm-8">
                <div><?php echo $details->description ?></div>
                  <?php if ($about_url):
                    print $about_url;
                  endif; ?>
                </div>
            </div>
            <div class="row"><br /></div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <div class="mails-block <?php echo !$is_logged ? 'single' : ''; ?>">
                    <?php $email = $details->field_dms_contact[LANGUAGE_NONE][0]['email'] ?>
                    <p>
                      <span class="email-icon">
                        <a href="mailto:<?php echo $email; ?>" target="_top"><?php echo $email; ?></a>
                      </span>
                    </p>
                    <?php if($is_logged): ?>
                      <p><span class="phone-icon"><?php echo $details->field_dms_phone[LANGUAGE_NONE][0]['safe_value'] ?></span></p>
                      <p><span class="schedule-icon"><?php echo $details->field_dms_schedule[LANGUAGE_NONE][0]['safe_value'] ?></span></p>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="links-block">
                    <ul class="links-block-list">
                      <?php foreach($details->field_dms_links[LANGUAGE_NONE] as $link): ?>
                          <li><a href="<?php echo $link['url'] ?>" target="_blank"><?php echo $link['title'] ?></a></li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="custom-block block block-block clearfix">
                    <div class="content">
                    <?php
                        if ($details_block):
                          print render($details_block['content']);
                        endif;
                    ?>
                    </div>
                  </div>
                </div>
            </div>
            <div class="row"><br /></div>
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h4 class="custom-block-title news">Latest News</h4>
                    <?php echo views_embed_view('dms_latest_ndv', 'block_latest_news_tid', $details->tid); ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <h4 class="custom-block-title documents">Latest Documents</h4>
                    <?php echo views_embed_view('dms_latest_ndv', 'block_latest_docs_tid', $details->tid); ?>
                    <h4 class="custom-block-title videos">Latest Videos</h4>
                    <?php echo views_embed_view('dms_latest_ndv', 'block_latest_videos_tid', $details->tid); ?>
                </div>
            </div>      
        </div>
      </div>
    </div>

    <?php if (!empty($suffix_display)): ?>
      <div class="row node-info">
        <div class="node-info-submitted col-lg-6 col-md-6 col-sm-6
        col-xs-12 col-lg-offset-6 col-md-offset-6 col-sm-offset-6">
          <div class="well well-sm node-submitted clearfix">
            <small>
              <?php print $user_picture; ?>
              <?php print $submitted; ?>
            </small>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div class="link-wrapper right">
      <?php print render($content['links']); ?>
    </div>

    <?php print render($content['comments']); ?>

  </div>
</div>
