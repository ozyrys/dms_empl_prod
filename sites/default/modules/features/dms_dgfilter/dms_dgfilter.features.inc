<?php

/**
 * @file
 * dms_dgfilter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dms_dgfilter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function dms_dgfilter_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function dms_dgfilter_node_info() {
  $items = array(
    'reports' => array(
      'name' => t('Reports'),
      'base' => 'node_content',
      'description' => t('Report nodes and information'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
