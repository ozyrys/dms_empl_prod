<?php

/**
 * @file
 * dms_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function dms_core_user_default_roles() {
  $roles = array();

  // Exported role: structure administrator.
  $roles['structure administrator'] = (object) array(
    'name' => 'structure administrator',
    'weight' => 7,
  );

  // Exported role: user administrator.
  $roles['user administrator'] = (object) array(
    'name' => 'user administrator',
    'weight' => 6,
  );

  return $roles;
}
