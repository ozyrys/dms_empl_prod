<?php

/**
 * @file
 * dms_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function dms_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'autologout_timeout';
  $strongarm->value = '3600';
  $export['autologout_timeout'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-capitalizator_ignored_words';
  $strongarm->value = array(
    'of' => 'of',
    'and' => 'and',
    'or' => 'or',
    'de' => 'de',
    'del' => 'del',
    'y' => 'y',
    'o' => 'o',
    'for' => 'for',
  );
  $export['easy_breadcrumb-capitalizator_ignored_words'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-disable_drupal_breadcrumb';
  $strongarm->value = 1;
  $export['easy_breadcrumb-disable_drupal_breadcrumb'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-excluded_paths';
  $strongarm->value = array(
    'search' => 'search',
    'search/node' => 'search/node',
  );
  $export['easy_breadcrumb-excluded_paths'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-home_segment_title';
  $strongarm->value = 'Defis';
  $export['easy_breadcrumb-home_segment_title'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-title_from_page_when_available';
  $strongarm->value = 1;
  $export['easy_breadcrumb-title_from_page_when_available'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'easy_breadcrumb-title_segment_as_link';
  $strongarm->value = 1;
  $export['easy_breadcrumb-title_segment_as_link'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'fullcalendar_path';
  $strongarm->value = 'sites/dms/libraries/fullcalendar';
  $export['fullcalendar_path'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mailsystem_theme';
  $strongarm->value = 'dms';
  $export['mailsystem_theme'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nexteuropa_varnish_default_purge_rule';
  $strongarm->value = TRUE;
  $export['nexteuropa_varnish_default_purge_rule'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_pattern';
  $strongarm->value = '[node:source:title]';
  $export['pathauto_node_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'roleassign_roles';
  $strongarm->value = array(
    6 => '6',
    5 => '5',
    7 => '7',
    9 => '9',
    3 => 0,
    4 => 0,
  );
  $export['roleassign_roles'] = $strongarm;

  return $export;
}
