<?php
/**
 * @file
 * dms_qg.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function dms_qg_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function dms_qg_node_info() {
  $items = array(
    'quick_guide' => array(
      'name' => t('Quick guide'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
