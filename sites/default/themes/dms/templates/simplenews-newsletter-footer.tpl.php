<?php

/**
 * @file
 * Default theme implementation to format the simplenews newsletter footer.
 *
 * Copy this file in your theme directory to create a custom themed footer.
 * Rename it to simplenews-newsletter-footer--[tid].tpl.php to override it for a
 * newsletter using the newsletter term's id.
 *
 * @todo Update the available variables.
 * Available variables:
 * - $build: Array as expected by render()
 * - $build['#node']: The $node object
 * - $language: language code
 * - $key: email key [node|test]
 * - $format: newsletter format [plain|html]
 * - $unsubscribe_text: unsubscribe text
 * - $test_message: test message warning message
 * - $simplenews_theme: path to the configured simplenews theme
 *
 * Available tokens:
 * - [simplenews-subscriber:unsubscribe-url]: unsubscribe url to be used as link
 *
 * Other available tokens can be found on the node edit form when token.module
 * is installed.
 *
 * @see template_preprocess_simplenews_newsletter_footer()
 */
?>
<div id="newsletter-footer">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" id="wrappertable" style="table-layout: fixed;">
    <tbody>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
              <tr>
                <td>
                  <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                      <tr>
                        <td colspan="2" width="1024">
                          <div align="right">
                            <a href="<?php print url('newsletter-subscriptions'); ?>">more news</a>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024" style="padding:10px; color:rgb(0, 102, 153); font-weight: bold; background-color: rgb(204, 224, 235);">More Information</td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024">
                          Visit our <a href="<?php print url('<front>'); ?>">portal</a> for further information on the Direct Management System.
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024" height="18">&nbsp;</td>
                      </tr>
                      <tr>
                        <td id="newsletter-footer-menu" style="border-top: 1px solid rgb(0, 102, 153); border-bottom: 1px solid rgb(0, 102, 153);">
                          <a href="<?php print url('<front>'); ?>" style="text-decoration: none;"><?php print theme('image', array('path' => 'http://ec.europa.eu/social/images/newsletter/bottom-menu-small.png')); ?></a>
                        </td>
                        <td style="border-top: 1px solid rgb(0, 102, 153); border-bottom: 1px solid rgb(0, 102, 153); border-right: 1px solid rgb(0, 102, 153);">
                          <a href="<?php print url('newsletter-subscriptions'); ?>">News</a> | <a href="<?php print url('events'); ?>">Events</a> | <a href="http://ec.europa.eu/social/main.jsp?catId=629&amp;langId=en">Calls for proposals</a>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024" height="18"></td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024" style="padding:10px; background-color:#0065A2; color: rgb(255, 255, 255);">
                          Publisher: <a href="http://ec.europa.eu/index_en.htm" style="color: rgb(255, 255, 255);">European Commission</a>, <a href="http://ec.europa.eu/social/home.jsp?langId=en" style="color: rgb(255, 255, 255);">DG Employment, Social Affairs &amp; Inclusion (DEFIS Systems)</a>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" width="1024" style="padding:10px; background-color:#0065A2; color: rgb(255, 255, 255);">
                           <a href="<?php print url('node/201'); ?>" style="color: rgb(255, 255, 255);">Contact</a> | <a href="<?php print url('newsletter-subscriptions'); ?>" style="color: rgb(255, 255, 255);">Unsubscribe</a> | <a href="http://ec.europa.eu/geninfo/legal_notices_en.htm" style="color: rgb(255, 255, 255);">Legal notice</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>

<?php if (!$opt_out_hidden): ?>
  <?php if ($format == 'html'): ?>
    <p class="newsletter-footer"><a href="[simplenews-subscriber:unsubscribe-url]"><?php print $unsubscribe_text; ?></a></p>
  <?php else: ?>
  -- <?php print $unsubscribe_text; ?>: [simplenews-subscriber:unsubscribe-url]
  <?php endif; ?>
<?php endif; ?>

<?php if ($key == 'test'): ?>
  <?php print $test_message; ?>
<?php endif; ?>
