<?php

/**
 * @file
 * Default theme implementation to format the simplenews newsletter body.
 *
 * Copy this file in your theme directory to create a custom themed body.
 * Rename it to override it. Available templates:
 *   simplenews-newsletter-body--[tid].tpl.php
 *   simplenews-newsletter-body--[view mode].tpl.php
 *   simplenews-newsletter-body--[tid]--[view mode].tpl.php
 * See README.txt for more details.
 *
 * Available variables:
 * - $build: Array as expected by render()
 * - $build['#node']: The $node object
 * - $title: Node title
 * - $language: Language code
 * - $view_mode: Active view mode
 * - $simplenews_theme: Contains the path to the configured mail theme.
 * - $simplenews_subscriber: The subscriber for which the newsletter is built.
 *   Note that depending on the used caching strategy, the generated body might
 *   be used for multiple subscribers. If you created personalized newsletters
 *   and can't use tokens for that, make sure to disable caching or write a
 *   custom caching strategy implemention.
 *
 * @see template_preprocess_simplenews_newsletter_body()
 */
?>
<div id="newsletter">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" id="wrappertable" style="table-layout: fixed;">
    <tbody>
      <tr>
        <td height="50" valign="top">
          <a href="http://ec.europa.eu/">European Commission </a> » <a href="http://ec.europa.eu/social/home.jsp?langId=en"> Employment, Social Affairs &amp; Inclusion </a>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
              <tr>
                <td>
                  <table cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                      <tr>
                        <td width="1024">
                          <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                              <tr>
                                <td id="logo" rowspan="2" style="width:212px;" valign="bottom">
                                  <!-- Site logo -->
                                  <?php if (!empty($logo)): ?>
                                    <img alt="<?php print t('European Commission logo'); ?>" id="logo-img" src="<?php print $logo; ?>" />
                                  <?php endif; ?>
                                </td>
                                <td valign="bottom" height="85" style="padding-left:20px;">
                                  <!-- Site name -->
                                  <div id="main-title"><?php print $site_name; ?></div>
                                </td>
                              </tr>
                              <tr>
                                <td valign="top" style="background-color:#0065A2; padding-left:20px;">
                                  <div id="sub-title"><?php print $site_slogan; ?></div> 
                                  <div id="fixed-min-width-maker">......................................................................................................................................................................................................</div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      <?php if (!empty($menu_links)): ?>
                      <tr>
                        <td>
                          <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody><tr>
                              <td>
                                <div id="menu-links">
                                  <?php print $menu_links; ?>
                                  <div id="menu-links-bottom-space">|</div>
                                </div>
                              </td>
                            </tr>
                          </tbody></table>
                        </td>
                      </tr>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
  
  <div id="banner">
    <?php if (!empty($banner)): ?>
      <img width="100%" alt="<?php print t('Banner'); ?>" id="banner-img" src="<?php print $banner; ?>" />
    <?php endif; ?>
  </div>
  
  <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tbody>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background: #f9f9f9;">
            <tbody>
              <tr>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                      <tr>
                        <td>
                          <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                              <tr>
                                <td>
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td style="padding-bottom: 20px;"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td width="1024">
                                          <!-- Newsletter body -->
                                          <?php if (!empty($title)): ?>
                                            <h2><?php print $title; ?></h2>
                                          <?php endif; ?>
                                          <?php if (!empty($build)): ?>
                                            <?php print render($build); ?>
                                          <?php endif; ?>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td style="padding-bottom: 15px;"></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>
