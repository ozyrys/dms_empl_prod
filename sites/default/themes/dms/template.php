<?php

/**
 * @file
 * template.php
 *
 * This theme comes with a neat solution for keeping this file as clean as
 * possible while the code grows.
 *
 * Any custom [pre]process, theme and hook functions can (rather than directly
 * in this template.php) be placed in its specific folder in a file named as
 * such:
 *
 * [THEMENAME]_[pre]process_html() = system.[pre]process.inc
 * [THEMENAME]_[pre]process_page() = system.[pre]process.inc
 * [THEMENAME]_[pre]process_block() = block.[pre]process.inc
 * [THEMENAME]_[pre]process_node() = node.[pre]process.inc
 * [THEMENAME]_[pre]process_field() = field.[pre]process.inc
 * [THEMENAME]_menu_tree() = menu.theme.inc
 * [THEMENAME]_block_info_alter = block.hook.inc
 * [THEMENAME]_form_alter() = system.hook.inc
 * etc.
 */

require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'dms') . '/preprocess/newsletter.preprocess.inc';
require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'dms') . '/preprocess/search.preprocess.inc';
require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'dms') . '/preprocess/system.preprocess.inc';
