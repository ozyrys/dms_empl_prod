<?php

/**
 * @file
 * system.preprocess.inc
 *
 * All preprocess functions related to "system" placeholder live in this file.
 */

/**
 * Preprocesses variables for default html templates.
 *
 * @param array $vars
 *   An associative array of variables to merge with defaults.
 *
 * @see template_preprocess_page()
 */

/**
 * Preprocess variables for page.tpl.php.
 */
function dms_preprocess_page(&$variables) {
  // Add new page template suggestion based on node type.
  if (isset($variables['node'])) {
    // If the node type is "blog" the template suggestion will be
    // "page--node--blog.tpl.php".
    $new_suggestion = 'page__node__' . $variables['node']->type;
    // Place new template suggestion before page__node__edit.
    if (!empty($variables['theme_hook_suggestions'][0])) {
      foreach ($variables['theme_hook_suggestions'] as $k => $suggestion) {
        if ($suggestion == 'page__node__edit') {
          array_splice($variables['theme_hook_suggestions'], $k, 0, $new_suggestion);
          break;
        }
      }
      // I there was no page__node__edit theme suggestion then add new theme
      // suggestion at the end.
      if (!in_array($new_suggestion, $variables['theme_hook_suggestions'])) {
        $variables['theme_hook_suggestions'][] = $new_suggestion;
      }
    }
  }
  // Apply active trails for each section.
  $sections = array(
    'defis',
    'bvf',
    'coli',
    'coli2',
    'swim',
    'publigrant',
    'external-expert',
    'finap',
    'requests',
    'reports',
    'newsletter',
  );
  $css = '';
  foreach ($sections as $section) {
    $css .= '.section-' . $section . ' .link-' . $section . ',.section-' . $section . ' .om-link.link-' . $section . ':hover,.section-' . $section . ' .om-link.link-' . $section . ':focus{background-color:#074A8B !important;}';
  }
  drupal_add_css($css, 'inline');

}
